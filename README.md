Trabalho da disciplina de engenharia de software
Lucas de Lima Sousa Bezerra
150137028

Templates usados: 

* Descrição de processo definido para o projeto (project defiged process). => OpenUp template
* Plano de projeto (project plan). =>  OpenUp project plan template
* Planos de iterações (iteraction plan). => OpenUp iteraction plan template
* Documento de visão (vision). => OpenUp vision document template
* Modelo de casos de uso (use-case model) ou documentação das estórias de usuários (user stories). => OpenUp use-case template
* Lista de ferramentas (tools). => OpenUp tools template
* Descrição de arquitetura de software (architecture notebook). => OpenUp architecture notebook template 
* Descrição de infraestrutura de implantação (infrastructure). => Modelo base do template de architecture notebook, adpatado para Descrever a infraestrutura
* Descrição do processo de controle de versões adotado. => Modelo base do template de project plan, adpatado para Descrever o processo de controle de versão
* Comprovação do uso de uma ferramenta para controle de versões. => Prints tiradas do repositório do trabalho
* Descrição do processo de teste adotado. => OpenUp e Celepar templates 
* Teste fumaça (smoke test) composto por casos de teste. => OpenUp test case template
